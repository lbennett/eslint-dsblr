const fs = require('fs');
const CLIEngine = require('eslint').CLIEngine;

const cliEngine = new CLIEngine({ allowInlineConfig: false });

const disableString = '/* eslint-disable */';
const disableRegex = new RegExp(`^${disableString.replace(/\/|\*/g, '\\$&')}`);

class Dsblr {
  run() {
    this.runEslint();
    this.results.forEach(this.disableInline.bind(this));
  }

  runEslint() {
    this.maxLen = cliEngine.getConfigForFile('.').rules['max-len'][1];
    this.results = cliEngine.executeOnFiles(['.']).results
      .filter(result => result.messages.length > 0);
  }

  disableInline(result) {
    let source = result.source;
    if (disableRegex.test(source)) {
      source = source.replace(disableRegex, this.disableString(result));
    } else {
      source = `${this.disableString(result)}\n\n${source}`;
    }
    Dsblr.overwriteSources({ source, path: result.filePath });
  }

  disableString(result) {
    let rulesString = result.messages.reduce(Dsblr.rulesReducer, '');
    if ((rulesString.length + disableString.length + 1) > this.maxLen) rulesString += ', max-len';
    return disableString.replace(/\*\/$/, `${rulesString} $&`);
  }

  static overwriteSources(file) {
    fs.writeFile(file.path, file.source, (err) => { if (err) throw err; });
  }

  static rulesReducer(rules, message, index) {
    /* eslint-disable no-param-reassign */
    if (!rules.includes(message.ruleId)) {
      if (index > 0) rules += ', ';
      rules += message.ruleId;
    }
    return rules;
    /* eslint-enable no-param-reassign */
  }
}

const dsblr = new Dsblr();

dsblr.run();
